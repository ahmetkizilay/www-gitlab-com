---
layout: markdown_page
title: "Travel"
---

## Booking Travel

The company will cover all work related travel costs.
A few important things to note:

1. Whenever it is possible, please book your own travel, using a company credit card if 
you have one, or submitting the receipt to ap@gitlab.com for reimbursement if you 
do not have a company credit card. If using a credit card is not an option at all, 
please send a message to travel@gitlab.com to get help in booking your travel.
1. The company can accommodate your custom request. It is OK to stay longer on 
your trip. However, the extra days will not be covered by the company.

## Expenses While Travelling

1. The company will pay for lodging and meals during the part of the trip
that is work related. Depending on the distance of your travel, this can include
one day before and one day after the work related business. For example, if you
are attending a 3 day conference in a jetlag-inducing location, the company will
cover your lodging and meals those 3 days as well as one day before and one day after.
1. Meals and lodging are covered. The "spend it like it is your own money" rule applies here except 
if explicitly noted otherwise. For example, the limits for during the Austin Summit
are detailed on the [Austin Summit page](https://dev.gitlab.org/summit_group_2016/Austin-Summit-2016-project).
1. Always bring a credit card with you when travelling for company business if you have one.
1. Hotels will expect you to have a physical credit card to present upon check-in. 
This credit card will be kept on file for the duration of your stay. Even if your lodging 
was pre-paid by the company or by using a company credit card, the Hotel may still
require a card to be on file for "incidentals".
1. If you incur any work-travel related expenses (on your personal card or a GitLab 
company card), please make sure to save the original receipt. 
1. When your trip is complete, please file an expense report via Expensify. 
Expensify steps can be found http://help.expensify.com/getting-started/ 

See the section on [Spending Company Money](https://about.gitlab.com/handbook/#spending-company-money)
on the handbook for more details on the expense policies.


